# PIXEL PERFECT Translation by Fabian Haeckermann (YEAR 2015)#

This is a Basic Oldfashion design translated to HTML CSS (LESS) & Responsive

### Tech ? ###

* LESS
* CSS
* HTML

### How do I get set up? ###

- clone repo
- open design.html(is just a page that includes design.jpg)
- run index.html (this is the actual HTML & CSS)


### Questions ? ###

fabianhaeckermann@gmail.com